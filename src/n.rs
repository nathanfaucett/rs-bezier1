use core::ops::{Mul, Sub};

use cast_trait::Cast;
use num_traits::{FromPrimitive, One, Zero};

use cubic::cubic;
use linear::linear;
use quadratic::quadratic;

#[inline]
pub fn n<T, F>(points: &[T], t: &F) -> T
where
    T: Clone + Cast<F> + Zero,
    F: Clone
        + One
        + Zero
        + FromPrimitive
        + PartialOrd
        + Cast<T>
        + Sub<F, Output = F>
        + Mul<F, Output = F>,
    for<'a, 'b> &'a F: Sub<&'b F, Output = F> + Mul<&'b F, Output = F>,
{
    if t <= &F::zero() {
        match points.len() {
            0 => T::zero(),
            _ => points[0].clone(),
        }
    } else if t >= &F::one() {
        match points.len() {
            0 => T::zero(),
            n => points[n - 1].clone(),
        }
    } else {
        match points.len() {
            0 => T::zero(),
            1 => points[0].clone(),
            2 => linear(&points[0], &points[1], t),
            3 => cubic(&points[0], &points[1], &points[2], t),
            4 => quadratic(&points[0], &points[1], &points[2], &points[3], t),
            n => casteljau(points, n - 1, 0, t),
        }
    }
}

#[inline]
fn casteljau<T, F>(points: &[T], i: usize, j: usize, t: &F) -> T
where
    T: Clone + Cast<F>,
    F: Clone + One + Zero + PartialOrd + Cast<T> + Sub<F, Output = F> + Mul<F, Output = F>,
    for<'a, 'b> &'a F: Sub<&'b F, Output = F> + Mul<&'b F, Output = F>,
{
    if i == 0_usize {
        points[j].clone()
    } else {
        let p0 = casteljau(points, i - 1, j, t);
        let p1 = casteljau(points, i - 1, j + 1, t);

        let x0 = p0.clone().cast();
        let x1 = p1.clone().cast();

        ((F::one() - t.clone()) * x0 + t.clone() * x1).cast()
    }
}

#[test]
fn test_n() {
    assert_eq!(n::<isize, f32>(&[], &0.0), 0);
    assert_eq!(n::<isize, f32>(&[], &0.5), 0);
    assert_eq!(n::<isize, f32>(&[], &1.0), 0);

    assert_eq!(n(&[0, 150, 200, 0], &0.25), 91);
    assert_eq!(n(&[0, 150, 200, 0], &0.5), 131);
    assert_eq!(n(&[0, 150, 200, 0], &0.75), 105);

    assert_eq!(n(&[0, 150, 200, 50, 0], &0.25), 106);
    assert_eq!(n(&[0, 150, 200, 50, 0], &0.5), 124);
    assert_eq!(n(&[0, 150, 200, 50, 0], &0.75), 69);
}
