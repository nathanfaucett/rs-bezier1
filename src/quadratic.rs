use core::ops::{Mul, Sub};

use cast_trait::Cast;
use num_traits::{FromPrimitive, One, Zero};

#[inline]
pub fn quadratic<T, F>(p0: &T, p1: &T, p2: &T, p3: &T, t: &F) -> T
where
    T: Clone + Cast<F>,
    F: Clone
        + One
        + Zero
        + FromPrimitive
        + PartialOrd
        + Cast<T>
        + Sub<F, Output = F>
        + Mul<F, Output = F>,
    for<'a, 'b> &'a F: Sub<&'b F, Output = F> + Mul<&'b F, Output = F>,
{
    if t <= &F::zero() {
        p0.clone()
    } else if t >= &F::one() {
        p3.clone()
    } else {
        let three = F::from_usize(3).unwrap();
        let one_min_t = &F::one() - t;
        let one_min_t_sq = &one_min_t * &one_min_t;
        let one_min_t_cb = &one_min_t_sq * &one_min_t;
        let t_sq: F = t * t;
        let t_cb = &t_sq * t;

        let x0 = p0.clone().cast();
        let x1 = p1.clone().cast();
        let x2 = p2.clone().cast();
        let x3 = p3.clone().cast();

        (one_min_t_cb * x0
            + three.clone() * one_min_t_sq * t.clone() * x1
            + three * one_min_t * t_sq * x2
            + t_cb * x3)
            .cast()
    }
}
#[test]
fn test_quadratic() {
    assert_eq!(quadratic(&0, &100, &200, &0, &0.25), 70);
    assert_eq!(quadratic(&0, &100, &200, &0, &0.5), 112);
    assert_eq!(quadratic(&0, &100, &200, &0, &0.75), 98);
}
