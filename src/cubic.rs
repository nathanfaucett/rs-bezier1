use core::ops::{Mul, Sub};

use cast_trait::Cast;
use num_traits::{FromPrimitive, One, Zero};

#[inline]
pub fn cubic<T, F>(p0: &T, p1: &T, p2: &T, t: &F) -> T
where
    T: Clone + Cast<F>,
    F: Clone + One + Zero + FromPrimitive + PartialOrd + Cast<T>,
    for<'a, 'b> &'a F: Sub<&'b F, Output = F> + Mul<&'b F, Output = F>,
{
    if t <= &F::zero() {
        p0.clone()
    } else if t >= &F::one() {
        p2.clone()
    } else {
        let two = F::from_usize(2).unwrap();
        let one_min_t = &F::one() - t;
        let one_min_t_sq = &one_min_t * &one_min_t;
        let t_sq = t * t;

        let x0 = p0.clone().cast();
        let x1 = p1.clone().cast();
        let x2 = p2.clone().cast();

        (one_min_t_sq * x0 + two * one_min_t * t.clone() * x1 + t_sq * x2).cast()
    }
}
#[test]
fn test_cubic() {
    assert_eq!(cubic(&0, &100, &200, &0.25), 50);
    assert_eq!(cubic(&0, &100, &200, &0.5), 100);
    assert_eq!(cubic(&0, &100, &200, &0.75), 150);
}
